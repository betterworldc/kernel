linux-surface4 Patch
====================

I am creating/maintaining this patch for me and people like me who would like
to have an updated kernel for their Microsoft Surface devices running Arch Linux.
As the original AUR package (https://aur.archlinux.org/packages/linux-surface4/)
is not currently up to date (4.14.15-1), this patch can be used to update it.

CHANGES
-------

* Using the kernel config from the original Arch Linux kernel (https://www.archlinux.org/packages/core/x86_64/linux/)
* Using kernel patches from the original Arch Linux kernel
* Downloading and using the "official" releases from https://github.com/jakeday/linux-surface
* Changes to the PKGBUILD to get closer to the original Arch Linux kernel
* Adding a .gitignore file to ignore built packages and downloaded artifacts

How to build this package
-------------------------

Clone this repository (see link at project page).

You are ready to go and build this package:
```bash
makepkg -sri
```
